
#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/ssid.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/nix-vector-helper.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/ipv4-flow-classifier.h"
#include "ns3/spectrum-module.h"
#include "ns3/spectrum-propagation-loss-model.h" 
#include "ns3/spectrum-channel.h" 
#include "ns3/spectrum-phy.h" 
#include "ns3/spectrum-wifi-helper.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"


// Default Network Topology
//
//       net1          net2
//
//       AP1 --------- AP2
//        |             |
//        |             |
//        |             |
//        |             |        
//       STA1          STA2                    
//   
using namespace ns3;

NS_LOG_COMPONENT_DEFINE("si");

int
main(int argc, char* argv[])
{
    auto start = std::chrono::high_resolution_clock::now(); // Start time

    bool verbose = false;
    bool tracing = false;


    LogComponentEnable("si", LOG_LEVEL_INFO);

   /*5 nodi Wifi -- 2 AP e 2 STA e un nodo aggiuntivo per osservare l'interferenza*/
    uint32_t nWifi = 2; 
    uint32_t nNet = 2; 
    uint32_t nInterference = 1;


    CommandLine cmd(__FILE__);
    //cmd.AddValue("nCsma", "Number of \"extra\" CSMA nodes/devices", nCsma);
    cmd.AddValue("nWifi", "Number of wifi STA devices", nWifi);
    cmd.AddValue("verbose", "Tell echo applications to log if true", verbose);
    cmd.AddValue("tracing", "Enable pcap tracing", tracing);

    cmd.Parse(argc, argv);

    // The underlying restriction of 18 is due to the grid position
    // allocator's configuration; the grid layout will exceed the
    // bounding box if more than 18 nodes are provided.
    if (nWifi > 18)
    {
        std::cout << "nWifi should be 18 or less; otherwise grid layout exceeds the bounding box"
                  << std::endl;
        return 1;
    }

    if (verbose)
    {
        LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
        LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);
    }


    //Crea i nodi della prima rete
    NodeContainer net1Nodes;
    net1Nodes.Create(nNet);

    //Crea i nodi della seconda rete
    NodeContainer net2Nodes;
    net2Nodes.Create(nNet);

    //Crea il nodo per osservare l'interferenza
    NodeContainer interferenceNode;
    interferenceNode.Create(nInterference);

    // Collega con csma due nodi AP e il nodo per osservare l'interferenza
    NodeContainer csmaNodes;
    csmaNodes.Add(net1Nodes.Get(1), net2Nodes.Get(1), interferenceNode.Get(0));
    
    CsmaHelper csma;
    csma.SetChannelAttribute("DataRate", StringValue("100Mbps"));
    csma.SetChannelAttribute("Delay", TimeValue(NanoSeconds(6560)));


    NetDeviceContainer csmaDevices;
    csmaDevices = csma.Install(csmaNodes);


    
    /*Utilizzando Yans*/
    
    YansWifiChannelHelper channel = YansWifiChannelHelper::Default();
    YansWifiPhyHelper phy;

    phy.SetChannel(channel.Create());
    phy.Set("ChannelSettings", StringValue("{1, 20, BAND_2_4GHZ, 0}"));

    
    WifiHelper wifi;
    wifi.SetStandard(WIFI_STANDARD_80211n);

    

    WifiMacHelper mac1;
    Ssid ssid1 = Ssid("ns-3-ssid-1");
    mac1.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid1), "ActiveProbing", BooleanValue(false));
    NetDeviceContainer sta1Device = wifi.Install(phy, mac1, net1Nodes.Get(0));
    mac1.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid1));
    NetDeviceContainer ap1Device = wifi.Install(phy, mac1, net1Nodes.Get(1));
    

    phy.Set("ChannelSettings", StringValue("{5, 20, BAND_2_4GHZ, 0}"));

    WifiMacHelper mac2;
    Ssid ssid2 = Ssid("ns-3-ssid-2");
    mac2.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid2), "ActiveProbing", BooleanValue(false));
    NetDeviceContainer sta2Device = wifi.Install(phy, mac2, net2Nodes.Get(0));
    mac2.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid2));
    NetDeviceContainer ap2Device = wifi.Install(phy, mac2, net2Nodes.Get(1));
    

       
    MobilityHelper mobility;

    mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                                  "MinX",
                                  DoubleValue(0.0),
                                  "MinY",
                                  DoubleValue(0.0),
                                  "DeltaX",
                                  DoubleValue(5.0),
                                  "DeltaY",
                                  DoubleValue(5.0),
                                  "GridWidth",
                                  UintegerValue(2),
                                  "LayoutType",
                                  StringValue("RowFirst"));

                                  
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(net1Nodes);
    mobility.Install(net2Nodes);

    InternetStackHelper stack;
    Ipv4NixVectorHelper nix;
    stack.SetRoutingHelper(nix);
    stack.Install(net1Nodes);
    stack.Install(net2Nodes);
    stack.Install(interferenceNode);
  

    Ipv4AddressHelper address;
    


    // Indirizzi IP per la rete 1 
    address.SetBase("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer sta1Interfaces = address.Assign(sta1Device);
    Ipv4InterfaceContainer ap1Interfaces = address.Assign(ap1Device);

    // Indirizzi IP per la rete 2
    address.SetBase("10.1.2.0", "255.255.255.0");
    Ipv4InterfaceContainer sta2Interfaces = address.Assign(sta2Device);
    Ipv4InterfaceContainer ap2Interfaces = address.Assign(ap2Device);
    
    //Indirizzi IP per csma
    address.SetBase("10.1.3.0", "255.255.255.0");
    Ipv4InterfaceContainer net3Interfaces = address.Assign(csmaDevices);

    UdpEchoServerHelper echoServer(9);

    ApplicationContainer interferenceServerApps = echoServer.Install(interferenceNode.Get(0));
    interferenceServerApps.Start(Seconds(1.0));
    interferenceServerApps.Stop(Seconds(20.0));

    // Installazione echoClient sul nodo STA della prima rete
    UdpEchoClientHelper echoClient1(net3Interfaces.GetAddress(2), 9);
    echoClient1.SetAttribute("MaxPackets", UintegerValue(1000));
    echoClient1.SetAttribute("Interval", TimeValue(Seconds(0.0001)));
    echoClient1.SetAttribute("PacketSize", UintegerValue(1024));

    ApplicationContainer clientApps1 = echoClient1.Install(net1Nodes.Get(nNet - 2));
    clientApps1.Start(Seconds(1.0));
    clientApps1.Stop(Seconds(20.0));

    // Installazione echoClient sul nodo STA della seconda rete
    UdpEchoClientHelper echoClient2(net3Interfaces.GetAddress(2), 9);
    echoClient2.SetAttribute("MaxPackets", UintegerValue(1000));
    echoClient2.SetAttribute("Interval", TimeValue(Seconds(0.0001)));
    echoClient2.SetAttribute("PacketSize", UintegerValue(1024));


    ApplicationContainer clientApps2 = echoClient2.Install(net2Nodes.Get(nNet - 2));
    clientApps2.Start(Seconds(2.0));
    clientApps2.Stop(Seconds(20.0));
    
    // Monitora con flowMonitor
    FlowMonitorHelper flowM;
    Ptr<FlowMonitor> monitor = flowM.InstallAll();


    Simulator::Stop(Seconds(40.0));
    Simulator::Run();
    

    
    // Report del FlowMonitor
  monitor->CheckForLostPackets();
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier>(flowM.GetClassifier());
  std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats();
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin(); i != stats.end(); ++i) {
    Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow(i->first);
    NS_LOG_INFO("Flow ID: " << i->first << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")\n");
    std::cout << "  Tx Bytes: " << i->second.txBytes << "\n";
    std::cout << "  Rx Bytes: " << i->second.rxBytes << "\n";
    std::cout << "  Throughput: " << i->second.rxBytes * 8.0 / (i->second.timeLastRxPacket.GetSeconds () - i->second.timeFirstTxPacket.GetSeconds ()) / 1024 / 1024 << " Mbps\n";
    std::cout << "  Delay: " << i->second.delaySum.GetSeconds()  << " s\n";
    std::cout << "  rx Packets: " << i->second.rxPackets  << " \n";
    std::cout << "  tx Packets: " << i->second.txPackets << " \n";
    std::cout << "  Packet Loss: " << i->second.lostPackets << "\n";
    NS_LOG_INFO("From " << t.sourceAddress << " To " << t.destinationAddress << std::endl);
    }



    Simulator::Destroy();

    auto end = std::chrono::high_resolution_clock::now(); // End time
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "Total simulation time: " << elapsed.count() << " seconds\n";

    return 0;
    
}


